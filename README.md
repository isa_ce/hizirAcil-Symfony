# (Bitirme Tezi / Senior Project)
- Acil servise gelen telefon çağrılarının bir kısmının sahte/gereksiz çağrılar olması, bu nedenle ambulans ekiplerinin gereksiz yere meşguliyetinin önlenmesi amaçlanmıştır.

- Technologies: HTML/CSS - JS - PHP - AJAX - REST
- Framework: Symfony 4.2 / PhpML

- UserId:admin
- Password:123456

![screenshot](https://user-images.githubusercontent.com/26628508/59523650-81034280-8eda-11e9-870e-be761ee95c2b.PNG)
